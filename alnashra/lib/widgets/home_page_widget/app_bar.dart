import 'package:alnashra/widgets/home_page_widget/my_drawer.dart';
import 'package:flutter/material.dart';
class AppBarWidget extends StatelessWidget {
final String title;

  const AppBarWidget({required this.title});

  @override
  Widget build(BuildContext context) {
    GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

    return AppBar(
        backgroundColor: Colors.blue[800],
        leading: IconButton(
            onPressed: () {
              _globalKey.currentState!.openDrawer();
            },
            icon: Icon(Icons.menu)),
        title: Text(title,
        style: TextStyle(color: Colors.white,fontWeight: FontWeight.w600,fontSize: 15),
        ),
        centerTitle: true,

      );

  }
}
