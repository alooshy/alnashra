// ignore_for_file: prefer_const_constructors, unnecessary_new

import 'package:alnashra/api/model/post/post.dart';
import 'package:alnashra/getx_controller/homecontroller.dart';
import 'package:flutter/material.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:page_indicator/page_indicator.dart';
import 'package:get/get.dart';
class HomeBody1 extends StatelessWidget {
  const HomeBody1({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final _controller = Get.find<HomeController>();

    return Container(
      height: size.height * 0.40,
      width: size.width,
      decoration: BoxDecoration(),
      child: new PageIndicatorContainer(
        length: 3,
        shape: IndicatorShape.circle(),
        indicatorColor: Colors.grey,
        indicatorSelectorColor: Colors.white,
        align: IndicatorAlign.bottom,
        child:
            PageView.builder(itemBuilder: (context, index) {
              return Stack(
                children: [
                  ClipRRect(
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(30),
                          bottomRight: Radius.circular(30)),
                      child: Image.asset(
                        'images/123.jpg',
                        width: size.width,
                        fit: BoxFit.cover,
                      )),
                  Container(
                    height: size.height * 0.40,
                    width: size.width,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(30),
                          bottomRight: Radius.circular(30)),
                      color: Colors.black.withOpacity(0.4),
                    ),
                  ),
                  Positioned(
                    bottom: 30,
                    left: 20,
                    child: Text(
                      'data asdasaf',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 30,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              );



            }, itemCount: 3,)),

    );
  }
}
