
import 'package:alnashra/getx_controller/home_view.dart';
import 'package:alnashra/views/articles_page.dart';
import 'package:alnashra/views/home_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MyDrawer extends StatelessWidget {
  const MyDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var selItem=['En','Ar'];
    return Container(
      child: Drawer(
        backgroundColor: Colors.blue[800],
        child: ListView(
          scrollDirection: Axis.vertical,
          children: [
            SizedBox(height: 10,),

            Image.asset(
                  'images/logo2.PNG',fit: BoxFit.contain,height: 150,width: 150,),

            SizedBox(height: 30,),
            InkWell(
              onTap: () {
              },
              child: ListTile(
                trailing: Icon(
                  Icons.home,
                  color: Colors.white,
                  size: 30,

                ),
                title: Text(
                  'Home Page',
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold,fontSize: 20),
                ),
              ),
            ),


            InkWell(
              onTap: () {
              },
              child: const ListTile(
                trailing: Icon(
                  Icons.menu_book_sharp,
                  color: Colors.white,
                  size: 30,
                ),
                title: Text(
                 'Articles'
                    ,
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold,fontSize: 20),
                ),
              ),
            ),
            InkWell(
              onTap: () {},
              child: const ListTile(
                trailing: Icon(
                  Icons.menu_book_sharp,
                  color: Colors.white,
                  size: 30,
                ),
                title: Text(
                  'Minstry'
                  ,
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold,fontSize: 20),
                ),
              ),
            ),


            InkWell(
              onTap: () {},
              child: ListTile(
                trailing: Icon(
                  Icons.article_outlined,
                  color: Colors.white,
                  size: 30,

                ),
                title: Text(
                  'News',
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold,fontSize: 20),
                ),
              ),
            ),

            InkWell(
              onTap: () {},
              child: ListTile(
                trailing: Icon(
                  Icons.video_collection_outlined,
                  color: Colors.white,
                  size: 30,

                ),
                title: Text(
                  'Videos',
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold,fontSize: 20),
                ),
              ),
            ),

            InkWell(
              onTap: () {},
              child: ListTile(
                trailing: Icon(
                  Icons.call,
                  color: Colors.white,
                  size: 30,

                ),
                title: Text(
                  'Contact us',
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold,fontSize: 20),
                ),
              ),
            ),
           Row(
             mainAxisAlignment: MainAxisAlignment.spaceBetween,
             children: [
               Text(
                 '   Language',
                 style: TextStyle(
                     color: Colors.white, fontWeight: FontWeight.bold,fontSize: 20),
               ),
               GetBuilder<HomeView>(
                 init: HomeView(),

                 builder: (controller)=>DropdownButton(
                   iconEnabledColor: Colors.white,
                   dropdownColor: Colors.blue[800],
                     value:controller.valueItem ,
                     items:selItem.map((e){
                       return DropdownMenuItem(


                           value: e,
                           child: Text(e,style:TextStyle(color:Colors.white,fontSize: 20)));
                     }).toList(),
                     onChanged:(index){
                       controller.onChangedItem('${index}');
                     },
               ),
               ),
             ],
           ),
          ],
        ),
      ),
    );
  }
}
