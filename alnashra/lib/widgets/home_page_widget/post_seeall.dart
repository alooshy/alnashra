import 'package:flutter/material.dart';

class PostSeeall extends StatelessWidget {
final String txt;

  const PostSeeall({required this.txt});
  @override
  Widget build(BuildContext context) {
    return Padding(padding: EdgeInsets.symmetric(horizontal: 20, vertical: 1),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(txt,style: TextStyle(color: Colors.black,fontSize: 18,fontWeight: FontWeight.w600),),
          TextButton(onPressed: (){}, child: Text('See all',style: TextStyle(color: Colors.red[900],fontSize: 15,fontWeight: FontWeight.w600)))
        ],
      ),
    );
  }
}
