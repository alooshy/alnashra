import 'package:flutter/material.dart';
class HomeBody3 extends StatelessWidget {
  const HomeBody3({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 15,vertical: 15),
      height:225,
      width: 250,
      padding: EdgeInsets.symmetric(vertical: 5,horizontal: 5),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
        boxShadow: [
          BoxShadow(
            blurRadius: 10,
            spreadRadius: 7,
            offset: Offset(1, 5),
            color: Colors.grey.withOpacity(0.3),

          ),
        ],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Image.asset('images/123.jpg',fit: BoxFit.cover,width: 250,height: 100,)),
          Text('Read more asca',style: Theme.of(context).textTheme.subtitle1),
          Container(
            padding: EdgeInsets.only(top: 3),
            height: 25,
            width: 240,
            decoration: BoxDecoration(
              color: Colors.blue[100],
              borderRadius: BorderRadius.circular(5),
            ),
            child:          Text('Read more ',textAlign: TextAlign.center,style: TextStyle(color: Colors.blue[900],fontSize: 15,),),

          ),
        ],
      ),
    );
  }
}
