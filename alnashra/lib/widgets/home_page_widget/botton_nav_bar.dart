// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:alnashra/getx_controller/home_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
class BottonNavBar extends StatefulWidget {
  int selindex=0;
  onTap(index){

  }
  @override
  _BottonNavBarState createState() => _BottonNavBarState();
}

class _BottonNavBarState extends State<BottonNavBar> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeView>(
      init:HomeView() ,
      builder:(controller)=> BottomNavigationBar(
        currentIndex: controller.navigatorvalue,
        type: BottomNavigationBarType.fixed,
        onTap: (index){
          controller.changedSelectedValue(index);
        },
        backgroundColor:Colors.blue[800],
        selectedItemColor: Colors.blue[300],
        unselectedItemColor: Colors.white,
        items: [

          BottomNavigationBarItem(
              icon: Icon(Icons.home,),
              title: Text('Home')

          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.video_collection_outlined,),
              title: Text('Videos')

          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.menu_book_sharp,),
              title: Text('Articles')
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.article_outlined ,),
              title: Text('News')
          ),



        ],
      ),
    );
  }
}
