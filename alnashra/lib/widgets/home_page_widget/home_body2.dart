// ignore_for_file: prefer_const_constructors

import 'package:alnashra/api/model/post/post.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class HomeBody2 extends StatelessWidget {
  const HomeBody2({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      margin: EdgeInsets.symmetric(horizontal: 15, vertical: 7),
      height: 100,
      width: size.width,
      padding: EdgeInsets.symmetric(vertical: 1, horizontal: 15),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
        boxShadow: [
          BoxShadow(
            blurRadius: 10,
            spreadRadius: 7,
            offset: Offset(1, 5),
            color: Colors.grey.withOpacity(0.3),
          ),
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Image.asset(
               'images/123.jpg',
                fit: BoxFit.cover,
                width: 75,
                height: 85,
              )),
          SizedBox(
            width: 10,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('asasfaasvasv',
    style: Theme.of(context).textTheme.subtitle1,
              ),
              Row(
                children: [
                  Text('data',
                      style: TextStyle(
                          color: Colors.blue[500],
                          fontSize: 15,
                          fontWeight: FontWeight.w600)),
                  SizedBox(
                    width: 132,
                  ),
                  TextButton(
                      onPressed: () {},
                      child: Row(
                        children: [
                          Text(
                            'Read more',
                            style: TextStyle(
                              color: Colors.blue[500],
                              fontSize: 15,
                            ),
                          ),
                          Icon(Icons.arrow_forward_rounded,
                              color: Colors.blue[500], size: 15),
                        ],
                      )),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
