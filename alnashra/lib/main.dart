// ignore_for_file: prefer_const_constructors

import 'package:alnashra/views/home_page.dart';
import 'package:alnashra/views/scroll_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'binding.dart';

import 'package:google_fonts/google_fonts.dart';
void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This home_page_widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      theme:ThemeData(
        textTheme: ThemeData.light().textTheme.copyWith(
          subtitle1:TextStyle(
              color: Colors.black,
              fontSize: 18,
              fontWeight: FontWeight.w600
          ),
        )
      ) ,
      initialBinding: controllerBinding(),
      debugShowCheckedModeBanner: false,
      home: ScrollPage(),
    );
  }
}
