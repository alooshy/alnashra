import 'package:get/get.dart';

import 'getx_controller/home_view.dart';
import 'getx_controller/homecontroller.dart';


class controllerBinding extends Bindings{
  @override
  void dependencies() {
    Get.put<HomeView>(HomeView());
    Get.put<HomeController>(HomeController());
  }
}