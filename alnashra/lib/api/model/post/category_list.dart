
class CategoryList
{
List<Category> categories;
CategoryList(this.categories);
  factory CategoryList.fromJson(List<dynamic> json)
  {
return    CategoryList(
        json.map((e) => Category.fromJson(e)).toList()
    );
  }
}
class Category {
  Category({
      int? id, 
      int? count, 
      String? description, 
      String? link, 
      String? name, 
      String? slug,}){
    _id = id;
    _count = count;
    _description = description;
    _link = link;
    _name = name;
    _slug = slug;
}

  Category.fromJson(dynamic json) {
    _id = json['id'];
    _count = json['count'];
    _description = json['description'];
    _link = json['link'];
    _name = json['name'];
    _slug = json['slug'];
  }
  int? _id;
  int? _count;
  String? _description;
  String? _link;
  String? _name;
  String? _slug;

  int? get id => _id;
  int? get count => _count;
  String? get description => _description;
  String? get link => _link;
  String? get name => _name;
  String? get slug => _slug;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['count'] = _count;
    map['description'] = _description;
    map['link'] = _link;
    map['name'] = _name;
    map['slug'] = _slug;
    return map;
  }

}