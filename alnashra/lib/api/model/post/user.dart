
class UsersList
{
  List<UsersModel> users;
  UsersList(this.users);
  factory UsersList.fromJson(List<dynamic> json)
  {
    return    UsersList(
        json.map((e) => UsersModel.fromJson(e)).toList()
    );
  }
}
class UsersModel {
  UsersModel({
    int? id,
    String? name,
    String? url,
    String? description,
    String? link,
    String? slug,
    String? href,}){
    _id = id;
    _name = name;
    _url = url;
    _description = description;
    _link = link;
    _slug = slug;
    _href = href;
  }

  UsersModel.fromJson(dynamic json) {
    _id = json['id'];
    _name = json['name'];
    _url = json['url'];
    _description = json['description'];
    _link = json['link'];
    _slug = json['slug'];
    _href = json['href'];
  }
  int? _id;
  String? _name;
  String? _url;
  String? _description;
  String? _link;
  String? _slug;
  String? _href;

  int? get id => _id;
  String? get name => _name;
  String? get url => _url;
  String? get description => _description;
  String? get link => _link;
  String? get slug => _slug;
  String? get href => _href;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['name'] = _name;
    map['url'] = _url;
    map['description'] = _description;
    map['link'] = _link;
    map['slug'] = _slug;
    map['href'] = _href;
    return map;
  }

}