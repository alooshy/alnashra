import 'dart:convert';

class CommentsList
{
  List<CommentsModel> comment;
  CommentsList(this.comment);
  factory CommentsList.fromJson(List<dynamic> json)
  {
    return    CommentsList(
        json.map((e) => CommentsModel.fromJson(e)).toList()
    );
  }
}

class CommentsModel {
  CommentsModel({
    int? id,
    int? post,
    int? parent,
    int? author,
    String? authorName,
    String? authorUrl,
    String? date,
    String? dateGmt,
    Content? content,
    String? link,
    String? status,
    String? type,
    String? href,}){
    _id = id;
    _post = post;
    _parent = parent;
    _author = author;
    _authorName = authorName;
    _authorUrl = authorUrl;
    _date = date;
    _dateGmt = dateGmt;
    _content = content;
    _link = link;
    _status = status;
    _type = type;
    _href = href;
  }

  CommentsModel.fromJson(dynamic json) {
    _id = json['id'];
    _post = json['post'];
    _parent = json['parent'];
    _author = json['author'];
    _authorName = json['author_name'];
    _authorUrl = json['author_url'];
    _date = json['date'];
    _dateGmt = json['date_gmt'];
    _content = json['content'] != null ? Content.fromJson(json['content']) : null;
    _link = json['link'];
    _status = json['status'];
    _type = json['type'];
    _href = json['href'];
  }
  int? _id;
  int? _post;
  int? _parent;
  int? _author;
  String? _authorName;
  String? _authorUrl;
  String? _date;
  String? _dateGmt;
  Content? _content;
  String? _link;
  String? _status;
  String? _type;
  String? _href;

  int? get id => _id;
  int? get post => _post;
  int? get parent => _parent;
  int? get author => _author;
  String? get authorName => _authorName;
  String? get authorUrl => _authorUrl;
  String? get date => _date;
  String? get dateGmt => _dateGmt;
  Content? get content => _content;
  String? get link => _link;
  String? get status => _status;
  String? get type => _type;
  String? get href => _href;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['post'] = _post;
    map['parent'] = _parent;
    map['author'] = _author;
    map['author_name'] = _authorName;
    map['author_url'] = _authorUrl;
    map['date'] = _date;
    map['date_gmt'] = _dateGmt;
    if (_content != null) {
      map['content'] = _content?.toJson();
    }
    map['link'] = _link;
    map['status'] = _status;
    map['type'] = _type;
    map['href'] = _href;
    return map;
  }

}

/// rendered : "<p>لنفسه الراحة والسلام في الامجاد السماوية  وليكن ذكره موءبدا</p>\n"

class Content {
  Content({
    String? rendered,}){
    _rendered = rendered;
  }

  Content.fromJson(dynamic json) {
    _rendered = json['rendered'];
  }
  String? _rendered;

  String? get rendered => _rendered;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['rendered'] = _rendered;
    return map;
  }

}