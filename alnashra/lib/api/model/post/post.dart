import 'dart:convert';


import 'content.dart';
import 'excerpt.dart';
import 'title.dart';

class PostList
{
  List<Post> list=[];
  PostList(this.list);
  factory PostList.fromJson(List<dynamic> json)
  {
    var list = json.map((e) => Post.fromMap(e)).toList();
    return PostList(list) ;
  }
}
class Post {
  int? id;
  DateTime? date;
  DateTime? dateGmt;
  String? link;
  Title? title;
  Content? content;
  Excerpt? excerpt;
  String? commentStatus;
  List<int>? categories;
  String? jetpackFeaturedMediaUrl;
  bool? jetpackSharingEnabled;

  Post({
    this.id,
    this.date,
    this.dateGmt,
    this.link,
    this.title,
    this.content,
    this.excerpt,
    this.commentStatus,
    this.categories,
    this.jetpackFeaturedMediaUrl,
    this.jetpackSharingEnabled,
  });

  @override
  String toString() {
    return 'Post(id: $id, date: $date, dateGmt: $dateGmt, link: $link, title: $title, content: $content, excerpt: $excerpt, commentStatus: $commentStatus, categories: $categories, jetpackFeaturedMediaUrl: $jetpackFeaturedMediaUrl, jetpackSharingEnabled: $jetpackSharingEnabled)';
  }

  factory Post.fromMap(Map<String, dynamic> data) => Post(
    id: data['id'] as int?,

    date: data['date'] == null
        ? null
        : DateTime.parse(data['date'] as String),
    dateGmt: data['date_gmt'] == null
        ? null
        : DateTime.parse(data['date_gmt'] as String),
    link: data['link'] as String?,
    title: data['title'] == null
        ? null
        : Title.fromMap(data['title'] as Map<String, dynamic>),
    content: data['content'] == null
        ? null
        : Content.fromMap(data['content'] as Map<String, dynamic>),
    excerpt: data['excerpt'] == null
        ? null
        : Excerpt.fromMap(data['excerpt'] as Map<String, dynamic>),
    commentStatus: data['comment_status'] as String?,
    jetpackFeaturedMediaUrl: data['jetpack_featured_media_url'] as String?,
    jetpackSharingEnabled: data['jetpack_sharing_enabled'] as bool?,
  );

  Map<String, dynamic> toMap() => {
    'id': id,
    'date': date?.toIso8601String(),
    'date_gmt': dateGmt?.toIso8601String(),
    'link': link,
    'title': title?.toMap(),
    'content': content?.toMap(),
    'excerpt': excerpt?.toMap(),
    'comment_status': commentStatus,
    'categories': categories,
    'jetpack_featured_media_url': jetpackFeaturedMediaUrl,
    'jetpack_sharing_enabled': jetpackSharingEnabled,
  };

  /// `dart:convert`
  ///
  /// Parses the string and returns the resulting Json object as [Post].
  factory Post.fromJson(String data) {

    return Post.fromMap(json.decode(data));
  }

  /// `dart:convert`
  ///
  /// Converts [Post] to a JSON string.
  String toJson() => json.encode(toMap());
}
