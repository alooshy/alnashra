import 'dart:convert';

class Excerpt {
  String? rendered;
  bool? protected;

  Excerpt({this.rendered, this.protected});

  @override
  String toString() => 'Excerpt(rendered: $rendered, protected: $protected)';

  factory Excerpt.fromMap(Map<String, dynamic> data) => Excerpt(
        rendered: data['rendered'] as String?,
        protected: data['protected'] as bool?,
      );

  Map<String, dynamic> toMap() => {
        'rendered': rendered,
        'protected': protected,
      };

  /// `dart:convert`
  ///
  /// Parses the string and returns the resulting Json object as [Excerpt].
  factory Excerpt.fromJson(String data) {
    return Excerpt.fromMap(json.decode(data) as Map<String, dynamic>);
  }

  /// `dart:convert`
  ///
  /// Converts [Excerpt] to a JSON string.
  String toJson() => json.encode(toMap());
}
