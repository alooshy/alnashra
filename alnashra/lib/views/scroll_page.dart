import 'package:alnashra/getx_controller/home_view.dart';
import 'package:alnashra/widgets/home_page_widget/botton_nav_bar.dart';
import 'package:alnashra/widgets/home_page_widget/my_drawer.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
class ScrollPage extends StatelessWidget {
  const ScrollPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return GetBuilder<HomeView>(builder: (controller)=>
    Scaffold(

      body: PageView(
        onPageChanged: controller.onPageChanged,
        scrollDirection: Axis.horizontal,
        children: [
          controller.currentScreen
        ],
      ),
      bottomNavigationBar: BottonNavBar(),
    ),
    );
  }
}
