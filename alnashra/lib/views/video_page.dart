import 'package:alnashra/widgets/home_page_widget/my_drawer.dart';
import 'package:flutter/material.dart';
class VideosPage extends StatelessWidget {

@override
Widget build(BuildContext context) {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  return Scaffold(
    key: _globalKey,
    drawer: MyDrawer(),
    appBar: AppBar(
      backgroundColor: Colors.blue[800],
      leading: IconButton(
          onPressed: () {
            _globalKey.currentState!.openDrawer();
          },
          icon: Icon(Icons.menu)),
      title: Text('Videos',
        style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 20),),
      centerTitle: true,
    ),
body: Center(child: Text('video page'),),
);
}
}