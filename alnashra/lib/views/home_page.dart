// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables


import 'package:alnashra/getx_controller/homecontroller.dart';
import 'package:alnashra/widgets/home_page_widget/home_body1.dart';
import 'package:alnashra/widgets/home_page_widget/home_body2.dart';
import 'package:alnashra/widgets/home_page_widget/home_body3.dart';
import 'package:alnashra/widgets/home_page_widget/my_drawer.dart';
import 'package:alnashra/widgets/home_page_widget/post_seeall.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
    final _controller = Get.find<HomeController>();

    Size size = MediaQuery.of(context).size;
    return Scaffold(
      drawer: MyDrawer(),

      key: _globalKey,
      appBar: AppBar(
        backgroundColor: Colors.blue[800],
        leading: IconButton(
            onPressed: () {
              _globalKey.currentState!.openDrawer();
            },
            icon: Icon(Icons.menu)),
        title: Image.asset(
          'images/mylogo.jpg',
          height: 45,
          width: 70,
          fit: BoxFit.cover,
        ),
        centerTitle: true,
      ),

      backgroundColor: Colors.white,
      body:Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
HomeBody1(),
          Container(
            height: size.height * 0.40,
            width: size.width,
            child: ListView(children:[
              PostSeeall(txt: 'New posts',),

              HomeBody2(),
              HomeBody2(),
              HomeBody2(),
              PostSeeall(txt: 'Latest News',),
 Container(
                      height: size.height * 0.35,
                      width: 100,
                      child: ListView(
                        scrollDirection: Axis.horizontal,
                        children: [
                          HomeBody3(),
                          HomeBody3(),
                        ],
                      ),
                    ),
            ]),


                ),

        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Icon(
          Icons.search,
          color: Colors.blue[900],
        ),
        backgroundColor: Colors.blue[200],
      ),
    );
  }
}
/*Column(
children: [
PostSeeall(),
HomeBody2(post:_controller.listpost[i] ,),

PostSeeall(),
/*  Container(
                      height: size.height * 0.35,
                      width: 100,
                      child: ListView(
                        scrollDirection: Axis.horizontal,
                        children: [
                          HomeBody3(),
                          HomeBody3(),
                        ],
                      ),
                    ),*/
],
);*/