import 'dart:convert';

import 'package:alnashra/api/model/post/category_list.dart';
import 'package:alnashra/api/model/post/comments.dart';
import 'package:alnashra/api/model/post/post.dart';
import 'package:alnashra/api/model/post/user.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;


class HomeController extends GetxController {
  var listpost = <Post>[].obs;
 var categories =  <Category>[].obs;
 var news =<Post>[].obs;
  var user =<UsersModel>[].obs;
  var comments =<CommentsModel>[].obs;



  Future<List<Post>> fetchPostByCategory(int page,int id) async {
    try {
      final response = await http
          .get(Uri.parse('https://annashra.org/wp-json/wp/v2/posts?categories=$id&page=$page'));

      // print(response.body);
      PostList posts = PostList.fromJson(jsonDecode(response.body));
      return posts.list;
    }catch(e){
      print(e);
      return [];
    }
  }
  Future<void> fetchUser() async {
    try {
      final response = await http
          .get(Uri.parse('https://annashra.org/wp-json/wp/v2/users'));

      // print(response.body);
      UsersList users = UsersList.fromJson(jsonDecode(response.body));
      user.addAll(users.users);
    }catch(e){
      print(e);
    }
  }

  Future<void> fetchComments() async {
    try {
      final response = await http
          .get(Uri.parse('https://annashra.org/wp-json/wp/v2/comments'));
      // print(response.body);
      CommentsList comm = CommentsList.fromJson(jsonDecode(response.body));
      comments.addAll(comm.comment);
    }catch(e){
      print(e);
    }
  }

  Future<void> fetchCategories() async {
    try {
      final response = await http
          .get(Uri.parse('https://annashra.org/wp-json/wp/v2/categories?page=3'));
      // print(response.body);
      CategoryList cats = CategoryList.fromJson(jsonDecode(response.body));
      categories.addAll(cats.categories);
    }catch(e){
      print(e);
    }
  }
  Future<Post> fetchAlbumModel1(int id) async {
   try {
      final response = await http
          .get(Uri.parse('https://annashra.org/wp-json/wp/v2/posts/$id'));
      print(response.body);
      Post _albumModel = Post.fromJson(jsonDecode(response.body));
      return _albumModel;
    }catch(e){
     print(e);
     return Post();

   }
  }
  Future<void> fetchAlbumModel() async {
    try {
      final response = await http
          .get(Uri.parse('https://annashra.org/wp-json/wp/v2/posts/'));
      // print(response.body);
      PostList _albumModel = PostList.fromJson(jsonDecode(response.body));
    listpost.addAll(_albumModel.list);
    }catch(e){
      print(e);

    }
  }

  Future<void> getLatestNews() async
  {
   var list= await fetchAlbumModel1(6163);
   news.add(list);

  }
  @override
  void onInit() {
    super.onInit();
     getLatestNews();
    fetchUser();
    fetchComments();
    fetchAlbumModel();
    fetchCategories();
  }

}
