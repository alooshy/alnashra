import 'package:alnashra/views/articles_page.dart';
import 'package:alnashra/views/home_page.dart';
import 'package:alnashra/views/news_page.dart';
import 'package:alnashra/views/video_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
class HomeView extends GetxController{
  int _navigatorvalue=0;
  String _valueItem='En';
  get valueItem=>_valueItem;
  void onChangedItem(String thevalue){
    _valueItem=thevalue;
    update();
  }
  PageController pageController=new PageController();
  get navigatorvalue=>_navigatorvalue;
  Widget _currentScreen=HomePage();
  get currentScreen=>_currentScreen;
  onPageChanged(int pageindex){
    this._navigatorvalue=pageindex;
  }
  void changedSelectedValue(int selvalue){
    _navigatorvalue=selvalue;
    switch(selvalue){
      case 0:{
        _currentScreen=HomePage();
        break;
      }
      case 1:{
        _currentScreen=VideosPage();
        break;
      }case 2:{
      _currentScreen=ArticlesPage();

      break;
    }case 3:{
      _currentScreen=NewsPage();

      break;
    }    }
    update();
  }
}